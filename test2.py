import cv2
import os
import numpy as np
import matplotlib.pyplot as plt
from scipy.signal import savgol_filter
from scipy.signal import find_peaks

PXL_PER_MM = 4608 / 13.2
DISTANCE_FROM_SCREEN = 425/1000 # m
ERROR_DISTANCE_FROM_SCREEN = 10/1000 # m
LASER_WAVELENGTH = 635e-9 # m
ERROR_LASER_WAVELENGTH = 40e-9 # m

#frequency = float(input('Frequenz in MHz: '))*1e6 #6.1e6
ERROR_FREQUENCY = 0.2e6


#path = input('Path:')

def eval_path(path, frequency):
    img_files = []
    if os.path.isfile(path):
        img_files = [path]
    elif os.path.isdir(path):
        img_files = os.listdir(path)
        img_files = [f'{path}\\{f}' for f in img_files if f[-4:] in ['.png', '.PNG', '.jpg', '.JPG']]

    speeds = []
    errors = []

    for p in img_files:
        #print(p)
        img = cv2.imread(p)#'Programm\img2.JPG')
        spec = np.sum(np.sum(img, 0),1)
        spec = savgol_filter(spec, 100, 3)
        #plt.plot((spec - min(spec))/1000)
        spec = spec[1:-1] - spec[0:-2]
        spec = savgol_filter(spec, 100, 3)
        #plt.plot(spec/10)
        spec = spec[1:-1] - spec[0:-2]
        spec = savgol_filter(spec, 100, 3)

        e,_ = find_peaks(-spec, prominence=18, distance=100, width=40, height=10)
        #e,_ = find_peaks(-spec, prominence=15, distance=100, width=30, height=7)
        #e,_ = find_peaks(-spec, prominence=10, distance=100, width=40, height=5)
        distances_px = (e - e[np.arange(len(e))-1])[1:]
        #med_distance = np.median(distances_px)
        #distances_px = [d for d in distances_px if abs(med_distance - d) < med_distance*0.05]


        if len(distances_px) < 2:
            print(f'Could not find enough maxima on {p}')
            continue
        avg_distance_px = np.average(distances_px)
        standard_deviation_px = np.sqrt(np.sum((avg_distance_px - distances_px)**2) / (len(distances_px)-1))
        avg_error_px = standard_deviation_px / np.sqrt(len(distances_px))

        avg_distance_mm = avg_distance_px/PXL_PER_MM
        avg_error_mm = avg_error_px/PXL_PER_MM

        #speed_of_sound = LASER_WAVELENGTH * frequency / np.sin(np.arctan(avg_distance_mm/DISTANCE_FROM_SCREEN))
        speed_of_sound = LASER_WAVELENGTH * frequency / (avg_distance_mm/1000) * DISTANCE_FROM_SCREEN
        error_speed_of_sound = np.sqrt((frequency / (avg_distance_mm/1000) * DISTANCE_FROM_SCREEN * ERROR_LASER_WAVELENGTH)**2
                                        + (LASER_WAVELENGTH / (avg_distance_mm/1000) * DISTANCE_FROM_SCREEN * ERROR_FREQUENCY)**2
                                        + (LASER_WAVELENGTH * frequency / (avg_distance_mm/1000) * ERROR_DISTANCE_FROM_SCREEN)**2
                                        + (LASER_WAVELENGTH * frequency / (avg_distance_mm/1000)**2 * DISTANCE_FROM_SCREEN * avg_error_mm/1000)**2)

        #print(f'x={avg_distance_mm}+-{avg_error_mm}')
        #print(f'c={speed_of_sound}+-{error_speed_of_sound}')
        #print()
        speeds.append(speed_of_sound)
        errors.append(error_speed_of_sound)

    if len(speeds) < 1:
        return None, None

    speeds = np.array(speeds)
    errors = np.array(errors)
    #print(errors)
    speed = np.sum(speeds / errors**2) / np.sum(1 / errors**2)
    error = np.sqrt(1 / np.sum(1 / errors**2))

    #print()
    #print(f'c_avg = {speed}+-{error}')
    return (speed, error)



#paths = [f'Bilder\\Wasser\\{f}' for f in ['20', '23', '55', '61', '63', '65']]
#frequencies = [2.0, 2.3, 5.5, 6.1, 6.3, 6.5]

paths = [f'Bilder\\Ethanol\\{f}' for f in ['23', '55', '61', '61_', '66', '205']]
frequencies = [2.3, 5.5, 6.1, 6.1, 6.6, 2.05]

s_ = []
e_ = []

for (p,f) in zip(paths, frequencies):
    s, e = eval_path(p, f*1e6)
    if s is None:
        print('Skipping this measurement')
        continue
    print(f'c_{f} = {s}+-{e}')
    s_.append(s)
    e_.append(e)

s_ = np.array(s_)
e_ = np.array(e_)

speed_final = np.sum(s_ / e_**2) / np.sum(1 / e_**2)
error_final = np.sqrt(1 / np.sum(1 / e_**2))
print()
print(f'c_avg = {speed_final}+-{error_final}')
